--Remove a previously inserted film from the inventory and all corresponding rental records
DELETE FROM "rental"
WHERE "inventory_id" IN (
  SELECT "inventory_id"
  FROM "inventory"
  WHERE "film_id" = (SELECT "film_id" 
				  FROM "film" 
                  WHERE "title" = '22 Mile'));


DELETE FROM "inventory" 
WHERE "film_id" = (SELECT "film_id" 
				  FROM "film" 
                  WHERE "title" = '22 Mile');
                

--Remove any records related to you (as a customer) from all tables except "Customer" and "Inventory"
DELETE FROM "payment"
WHERE "customer_id" IN (SELECT "customer_id" 
                      FROM "customer"
                      WHERE "first_name" = 'Nodirjon' AND last_name = 'Khudoyberdiev');

DELETE FROM rental 
WHERE "customer_id" IN (SELECT "customer_id" 
                      FROM "customer"
                      WHERE "first_name" = 'Nodirjon' AND last_name = 'Khudoyberdiev');

--Deleting film itself form film is not required that's why I didn't but Here I leave the sql command to delete film
--DELETE FROM "film"
--WHERE "title" = '22 Mile'; 
